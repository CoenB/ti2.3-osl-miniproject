import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:rpi_gpio/rpi_gpio.dart';
import 'package:rpi_gpio/rpi_hardware.dart' deferred as rpi;

Pin motionPin;
Pin ledPin;
Pin transmitterPin;

List<Timer> timers = [];
List<StreamSubscription> subscriptions = [];
LightState lightState = new LightState.off();

var rand = new Random();
bool randomValue = false;

main(List<String> arguments) async {
  if (isRaspberryPi) {
    await rpi.loadLibrary();
    Gpio.hardware = new rpi.RpiHardware();
    Gpio.instance;

    motionPin = pin(1, PinMode.input);
    ledPin = pin(2, PinMode.output);
    transmitterPin = pin(15, PinMode.output);

    print('Motion     : ${motionPin.description}');
    print('Led        : ${ledPin.description}');
    print('Transmitter: ${transmitterPin.description}');
  } else {
    print('Start producing random values');
    timers.add(new Timer.periodic(new Duration(seconds: 3), (t) {
      randomValue = rand.nextBool();
    }));
  }

  print('Start listening for changes');
  timers.add(new Timer.periodic(new Duration(milliseconds: 10), (t) async {
    if (!lightState.auto)
      return;

    bool newValue = isRaspberryPi ? motionPin.value == 1 : randomValue;
    if (lightState.on != newValue) {
      print('${(newValue ? 'Movement detected!' : 'Nobody around')}');
      await toggleLight(true, newValue);
    }
  }));

  print('Start server');
  HttpServer.bind(InternetAddress.ANY_IP_V4, 7891).then((server) {
    print('Server at address ${server.address.address}');
    subscriptions.add(server.listen((request) async {
      if (request.method == 'POST') {
        var decoded;

        try {
          decoded = await request
              .transform(UTF8.decoder.fuse(JSON.decoder))
              .first;
        } catch (e) {
          print('Request listen error: $e');
          return;
        }

        if (decoded.containsKey('auto') && decoded['auto'] is bool &&
            decoded.containsKey('on') && decoded['on'] is bool) {
          bool auto = decoded['auto'];
          bool on = decoded['on'];
          print('External request: set mode to ${(auto ? 'auto' : on ? 'on' : 'off')}');
          toggleLight(auto, on);
        } else {
          print('Json invalid');
        }

        addCorsHeaders(request.response);

      } else {
        print('External request: invalid method');
      }
      request.response.write('Hello world!');
      request.response.close();
    }));
  });

  new Timer(new Duration(seconds: 60), () {
    print('Stop listening to changes.');
    timers.forEach((t) => t.cancel());
    subscriptions.forEach((s) => s.cancel());
    toggleLight(false, false);
  });
}

void toggleLight(bool auto, bool on) async {
  if (isRaspberryPi)
    ledPin.value = on ? 1 : 0;

  lightState.auto = auto;
  lightState.on = on;
  String onStr = (on ? 'on' : 'off');

  if (isRaspberryPi) {
    await Process.run('./kaku', ['15', 'A', onStr]).then((result) {
      print('code ${result.exitCode}:');
      print(result.stdout);
    });
  }
}

void addCorsHeaders(HttpResponse response) {
  response.headers.add('Access-Control-Allow-Origin', '*');
  response.headers
      .add('Access-Control-Allow-Methods', 'POST, OPTIONS');
  response.headers.add('Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept');
}

class LightState {
  bool auto;
  bool on;

  LightState.off() {
    auto = false;
    on = false;
  }
}
